var map = L.map('mapid').setView([48.63458,-4.38138], 15);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);


map.scrollWheelZoom.disable();
map.removeControl(map.zoomControl);

let zoneActual = "";

let zones = {bzh: {id: "bzh", name: "Bretagne", coord : { latMax: 48.6, longMin :-4.61, longMax:-0.8, latMin :47 }},
            ire: {id: "ire", name: "Eire", coord : { latMax: 55.6071, longMin :-8.8261, longMax:-6.3262, latMin :52.1304}},
            burkina: {id: "burkina", name: "Burkina Faso", coord : { latMax: 14.9866, longMin :-4.6136, longMax:0.25, latMin :9.6786}},
            viet: {id: "viet", name: "Vietnam", coord : { latMax: 22.6958, longMin :106.5691, longMax: 106.7548, latMin :20.1652}},
            eus: {id: "eus", name: "Euskadi", coord : { latMax: 43.4822, longMin :-1.6320, longMax: -1.6461, latMin :43.0577}}
    
}

let markerVisible = {"ire": false, "bzh": false, "val": false, "burkina":false, "viet":false };

let found = false;


changeLieu();

AFRAME.registerComponent('registerevents', {
init: function () 
{
    let marker = this.el;

    marker.addEventListener('markerFound', function() {
        markerVisible[ marker.id ] = true;
        //console.log( markerVisible );
    });

    marker.addEventListener('markerLost', function() {
        markerVisible[ marker.id ] = false;
        // console.log( markerVisible );
    });
}
});

AFRAME.registerComponent('loop', 
{   
init: function()
{
    
},

tick: function (time, deltaTime) 
{
    // for convenience
    //console.log(markerVisible);
    
    //console.log(this.mapPlane);
    
    let mv = markerVisible, val = "val"
    bzh = "bzh", bf = "burkina", v = "viet", ire = "ire", eus ="eus";
        if(!found){
            if ( mv[bzh] && mv[val] ){ 
                validateChoice(bzh);
            }
            else if ( mv[ire] && mv[val] ){  
                validateChoice(ire);
            }
            else if ( mv[bf] && mv[val] ){  
                validateChoice(bf);
            }
            else if ( mv[v] && mv[val] ){  
                validateChoice(v);
            }
            else if ( mv[eus] && mv[val] ){  
                validateChoice(eus);
            }
        }

    /*  else if ( mv[r] && mv[y] && !mv[b] )
            this.box.setAttribute("color", "#FF8800");
        else if ( mv[r] && !mv[y] && mv[b] )
            this.box.setAttribute("color", "purple");
        else if ( mv[r] && !mv[y] && !mv[b] )
            this.box.setAttribute("color", "red");
        else if ( !mv[r] && mv[y] && mv[b] )
            this.box.setAttribute("color", "green");
        else if ( !mv[r] && mv[y] && !mv[b] )
            this.box.setAttribute("color", "yellow");
        else if ( !mv[r] && !mv[y] && mv[b] )
            this.box.setAttribute("color", "blue");
        else // if ( !mv[r] && !mv[y] && !mv[b] )
            this.box.setAttribute("color", "gray"); */
    
}
});

function wait(ms){
    var start = new Date().getTime();
    var end = start;
    while(end < start + ms*1000) {
        end = new Date().getTime();
    }
}

function changeLieu(){
    var numbZones = Object.keys(zones).length;
    let izoneChoosed = Math.floor(Math.random()*numbZones);   

    let zoneChoosed = zones[Object.keys(zones)[izoneChoosed]];

    while(zoneActual==zoneChoosed.id){
        numbZones = Object.keys(zones).length;
        izoneChoosed = Math.floor(Math.random()*numbZones);   
        zoneChoosed = zones[Object.keys(zones)[izoneChoosed]];
    }

    console.log(zoneChoosed);

    let lat = Math.random() * (zoneChoosed.coord.latMax - zoneChoosed.coord.latMin + 1) + zoneChoosed.coord.latMin;
    let long = Math.random() * (zoneChoosed.coord.longMax - zoneChoosed.coord.longMin + 1) + zoneChoosed.coord.longMin;

    zoneActual = zoneChoosed.id;

    map.panTo(new L.LatLng(lat, long));
    found = false;

}

function validateChoice(tag){
    if(zoneActual==tag){
        found = true;
        changeLieu();
        if (window.confirm("Felicitations ! La région était bien : " + zones[tag].name + " \n Veuillez cacher vos markers." )) { 
            found = false;
          }         
    }
}



